function bgChange() {
  var nav = document.getElementById('nav');
  var header = document.getElementById('header');
  var actuel = document.getElementById('actuel');
  var headertitle = document.getElementById('headertitle');
  nav.style.backgroundColor = '#ED213A';
  actuel.style.backgroundColor = '#ffffff';
  actuel.style.color = '#ED213A';
  header.style.backgroundColor = '#ffffff';
  headertitle.style.color = "black";
}
function bgActuelChange() {
  var nav = document.getElementById('nav');
  nav.style.backgroundColor = 'rgba(255, 255, 255)';
  var actuel = document.getElementById('actuel');
  var header = document.getElementById('header');
  actuel.style.backgroundColor = '#93291E';
  actuel.style.color = '#ffffff';
  header.style.color = '#ED213A';
}

function resetBg(){
  var nav = document.getElementById('nav');
  var headertitle = document.getElementById('headertitle');
  var header = document.getElementById('header');
  var actuel = document.getElementById('actuel');
  nav.style.backgroundColor = 'rgba(255, 255, 255)';
  actuel.style.backgroundColor = '#ED213A';
  actuel.style.color = '#ffffff';
  header.style.backgroundColor = '#ED213A';
  headertitle.style.color = "white";
}
