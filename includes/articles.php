<div id="article1" class="nouvelle">
<h3>Ils ont sortis une série audio et vidéo!</h3>
<img class="imagesarticle" src="../img/postapo.jpg" alt="">
<p>Les deux musiciens et acteurs ont sortis un album disponible partout où ils racontent une histoire post apocalyptique que Jack Black et Kyle Gass essayent de survive après qu'une bombe nucléaire ai tomber sur les États-Unis.
. Ne vous inquiété pas, elle est basé sur l'humour des deux, mais porter à avoir des parties avec des scènes sexuelles. Les dessins ont tous été réalisé par Jack Black. Voici l'annonce officielle de l'album:</p>
<iframe width="80%" height="600" src="https://www.youtube.com/embed/or5ZvsKY5-4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
<div id="article2" class="nouvelle">
<h3>Jack Black Et Jack White enregistrent ensemble.</h3>
<img src="https://images.radio-canada.ca/q_auto,w_1250/v1/ici-info/16x9/jack-white-jack-black.jpg" alt="">
<p>L’acteur et musicien Jack Black a annoncé avoir enregistré une chanson avec le musicien Jack White. Désormais, tout le monde se demande si la chanson va sortir sous le nom d'artiste « Jack Grey ».
Évidemment, c’est une blague qui a été reprise souvent au fil des années et des rares rencontres entre les deux artistes. Surtout depuis le début des années 2000, quand les deux Jack sont devenus de plus en plus populaires.
Après de nombreuses rumeurs, la collaboration est maintenant officielle. Durant un concert de son groupe de hard rock Tenacious D à Nashville, Jack Black a expliqué avoir rencontré Jack White pour un enregistrement, mais n'en a pas dit beaucoup plus.
"Nous sommes allés chez lui aujourd’hui et nous avons enregistré une chanson", a dit Jack Black sur scène. Aussi sur sa chaine youtube: <a href="https://www.youtube.com/channel/UCuriCa9loP_OsH75_5j8M5w"> JablinskiGames</a>, il a filmé l'arrivé et la sortie de la maison de Jack White. Comme le démontre cette <a href="https://youtu.be/sOace4JpbwI">video</a>, ou a cette <a href="https://youtu.be/sOace4JpbwI?t=600">extrait précis.</a>
Black a indiqué que la chanson sortirait prochainement.
La collaboration était dans l’air depuis quelque temps. Une photo de Jack White en compagnie des deux membres de Tenacious D a été publiée en juin dernier sur Instagram. « Nous avons engagé un nouveau vendeur », pouvait-on lire dans la publication.
Les deux hommes s'étaient aussi retrouvés dans un aéroport de Londres après que le groupe de White, les Raconteurs, eut joué au stade de Wembley. </p>
</div>
<div id="article3" class="nouvelle">
<h3>Jack Black Et Jack White enregistrent ensemble.</h3>
<img src="https://images.radio-canada.ca/q_auto,w_1250/v1/ici-info/16x9/jack-white-jack-black.jpg" alt="">
<p>L’acteur et musicien Jack Black a annoncé avoir enregistré une chanson avec le musicien Jack White. Désormais, tout le monde se demande si la chanson va sortir sous le nom d'artiste « Jack Grey ».
Évidemment, c’est une blague qui a été reprise souvent au fil des années et des rares rencontres entre les deux artistes. Surtout depuis le début des années 2000, quand les deux Jack sont devenus de plus en plus populaires.
Après de nombreuses rumeurs, la collaboration est maintenant officielle. Durant un concert de son groupe de hard rock Tenacious D à Nashville, Jack Black a expliqué avoir rencontré Jack White pour un enregistrement, mais n'en a pas dit beaucoup plus.
"Nous sommes allés chez lui aujourd’hui et nous avons enregistré une chanson", a dit Jack Black sur scène. Aussi sur sa chaine youtube: <a href="https://www.youtube.com/channel/UCuriCa9loP_OsH75_5j8M5w"> JablinskiGames</a>, il a filmé l'arrivé et la sortie de la maison de Jack White. Comme le démontre cette <a href="https://youtu.be/sOace4JpbwI">video</a>, ou a cette <a href="https://youtu.be/sOace4JpbwI?t=600">extrait précis.</a>
Black a indiqué que la chanson sortirait prochainement.
La collaboration était dans l’air depuis quelque temps. Une photo de Jack White en compagnie des deux membres de Tenacious D a été publiée en juin dernier sur Instagram. « Nous avons engagé un nouveau vendeur », pouvait-on lire dans la publication.
Les deux hommes s'étaient aussi retrouvés dans un aéroport de Londres après que le groupe de White, les Raconteurs, eut joué au stade de Wembley. </p>
</div>
<div id="article4" class="nouvelle">
<h3>Jack Black Et Jack White enregistrent ensemble.</h3>
<img src="https://images.radio-canada.ca/q_auto,w_1250/v1/ici-info/16x9/jack-white-jack-black.jpg" alt="">
<p>L’acteur et musicien Jack Black a annoncé avoir enregistré une chanson avec le musicien Jack White. Désormais, tout le monde se demande si la chanson va sortir sous le nom d'artiste « Jack Grey ».
Évidemment, c’est une blague qui a été reprise souvent au fil des années et des rares rencontres entre les deux artistes. Surtout depuis le début des années 2000, quand les deux Jack sont devenus de plus en plus populaires.
Après de nombreuses rumeurs, la collaboration est maintenant officielle. Durant un concert de son groupe de hard rock Tenacious D à Nashville, Jack Black a expliqué avoir rencontré Jack White pour un enregistrement, mais n'en a pas dit beaucoup plus.
"Nous sommes allés chez lui aujourd’hui et nous avons enregistré une chanson", a dit Jack Black sur scène. Aussi sur sa chaine youtube: <a href="https://www.youtube.com/channel/UCuriCa9loP_OsH75_5j8M5w"> JablinskiGames</a>, il a filmé l'arrivé et la sortie de la maison de Jack White. Comme le démontre cette <a href="https://youtu.be/sOace4JpbwI">video</a>, ou a cette <a href="https://youtu.be/sOace4JpbwI?t=600">extrait précis.</a>
Black a indiqué que la chanson sortirait prochainement.
La collaboration était dans l’air depuis quelque temps. Une photo de Jack White en compagnie des deux membres de Tenacious D a été publiée en juin dernier sur Instagram. « Nous avons engagé un nouveau vendeur », pouvait-on lire dans la publication.
Les deux hommes s'étaient aussi retrouvés dans un aéroport de Londres après que le groupe de White, les Raconteurs, eut joué au stade de Wembley. </p>
</div>
