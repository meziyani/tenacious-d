<?php
if(!isset($_GET['p'])){
  header('Location: http://'.$_SERVER['HTTP_HOST'].'/index.php?p=0');
}

include($_SERVER['DOCUMENT_ROOT'] . '/includes/pages.php');
$p = $_GET['p'];
$title = $pages[$p]->name;
?>
<!DOCTYPE html>
<head>
    <title>Tenacious D <?php echo '- '.$title ?></title>
    <link rel="stylesheet" type="text/css" href="style/normalize.css"/>
    <link rel="stylesheet" type="text/css" href="style/style.css"/>

    <?php if($p == 0 || $p == 2){?>
    <link rel="stylesheet" type="text/css" href="style/print.css" media="print" />
    <?php }?>
    
    <link rel="icon" href="/img/pick_of_destiny.png" />
    <meta name="viewport" content="width=device-width,
initial-scale=1, shrink-to-fit=no">
</head>
<body>
<header id="smallheader">
  <div class="hamburger-menu" onclick="">
  <div class="hamburger-bar"></div>
  <div class="hamburger-bar"></div>
  <div class="hamburger-bar"></div>
  </div>
  <nav id="smallnav">
    <ul>
      <?php
      //-------------CREATE MENU----.---------
      for($i = 0; $i < sizeof($pages); $i++){
          if($pages[$i]->menu == 0){
              echo '<a href="index.php?p='. $i .'">'.'<li>'. $pages[$i]->name .'</li></a>';
          }
      }
      ?>
    </ul>
  </nav>
</header>
<header id ="bigheader">
  <a href="http://<?=$_SERVER['HTTP_HOST']?>"><img src="http://<?=$_SERVER['HTTP_HOST']?>/img/logo.gif" alt="Logo"></a>
  <nav id="nav">
  <ul>
  <?php
  //-------------CREATE MENU----.---------
  for($i = 0; $i < sizeof($pages); $i++){
    if($pages[$i]->menu == 1){
      if($p == $i){
          echo '<a onmouseover="bgActuelChange()"
    onmouseout="resetBg()" href="index.php?p='. $i .'">'.'<li id="actuel">'. $pages[$i]->name .'</li></a>';
      } else {
          echo '<a onmouseover="bgChange()"
          onmouseout="resetBg()" href="index.php?p='. $i .'">'.'<li >'. $pages[$i]->name .'</li></a>';
      }
  }
  }
  ?>
  </ul>

</nav>
</div>
<div class="hamburger-menu" onclick="">
<div class="hamburger-bar"></div>
<div class="hamburger-bar"></div>
<div class="hamburger-bar"></div>


<script type="text/javascript" src="http://<?=$_SERVER['HTTP_HOST'] . '/scripts/menu.js'?>"></script>
<?php include($_SERVER['DOCUMENT_ROOT']."/includes/backtotop.php");?>
</header>
