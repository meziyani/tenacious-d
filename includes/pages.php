<?php
include($_SERVER['DOCUMENT_ROOT'] . '/objects/page.php');
$pages = array();
$pages[0] = new Page("Acceuil","acceuil.php","Page d'acceuil du site non officiel de Tenacious D.", 1);
$pages[1] = new Page("À propos","a-propos.php","Page d'information sur le groupe", 0);
$pages[2] = new Page("Actualités","actualites.php","Page sur les dernières nouvelles du groupe", 1);
$pages[3] = new Page("Discographie","discographie.php","Page sur la discographie du groupe", 1);
$pages[4] = new Page("Statistiques","statistiques.php","Page sur les statistiques du groupe", 1);
$pages[5] = new Page("Contact","contact.php","Forumulaire de contact", 0);
$pages[6] = new Page("Membres","membres.php","Liste des membres du groupe", 0);
$pages[7] = new Page("Sources","sources.php","Sources de nos informations", 0);
$pages[8] = new Page("Plan du site","plan-site.php","Forumulaire de contact", 0);
$pages[8] = new Page("Galerie","galerie.php","Galerie d'images", 1);
$pages[9] = new Page("Image","image.php","Visualiseur d'images", 3);
//Voir l'object page pour comprendre le constructeur
?>
