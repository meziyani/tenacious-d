<div class="container">


  <div class="membres">

  <div class="membre">

  <img src="../img/Jack_Black.jpg">

  <div class="description">

    <h2>Jack Black</h2>

  <p>Thomas Jacob Black, dit Jack Black, est un acteur, chanteur, producteur, compositeur, humoriste et scénariste américain, né le 28 août 1969 (50 ans) à Hermosa Beach, situé en Californie.

    Révélé au début des années 2000 en tête d'affiche des comédies L'Amour extra-large (où il jouait Hal Larson) et Rock Academy (où il jouait le professeur Dewey Finn), il a connu un succès international en 2005 avec le remake de King Kong (où il faisait Carl Denham), en 2006 avec la romance The Holiday (où il joue Miles) et le film portant sur son propre groupe de musique avec son collègue Kyle Gass Tenacious D in The Pick Of Destiny. Depuis 2008, il prête sa voix à Po Ping, le panda héros de la franchise d'animation Kung Fu Panda. Black mène également une carrière dans la musique en étant le chanteur principal du groupe de rock acoustique basée sur la comédie Tenacious D.</p>

  </div>

  </div>

  <div class="membre">

  <img src="../img/Kyle_Gass.jpg">

  <div class="description">

    <h2>Kyle Gass</h2>

    <p>Kyle Richard Gass est un acteur, scénariste, compositeur et producteur américain né le 14 juillet 1960 (59 ans) à Walnut Creek situé en Californie. Il est membre des groupes Tenacious D (avec Jack Black) et <a href="https://fr.wikipedia.org/wiki/Trainwreck">Trainwreck</a>. Il est souvent surnommé KG ou Kage.</p>
  </div>

  </div>
  </div>
  </div>

