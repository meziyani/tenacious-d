
<div class="fullSize centerItems">
<a href="#container" class="bouton">En savoir plus</a>
</div>

<main class="gridcontainer" id="container">
  <div class="video">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/X2zqiX6yL3I" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
  </div>
  <div class="extrait">
    <h1>Écoutez dès maintenant un extrait!</h1>
    <p>Un extrait de : "Post Apocalypto Theme"</p>
    <audio controls muted>
      <source src="../audio/Tenacious_D_POST_APOCALYPTO_THEME.mp3" type="audio/mp3"/>

      Votre navigateur ne supporte pas la balise audio.
    </audio>
  </div>
</main>
