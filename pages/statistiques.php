<div class="container">
  <div class="pageStatistiques">

    <h1>Statistiques</h1>
    <h2>Discographie</h2>
    <div class="statistiques">
      <div class="statistique">
        <img src="../img/Post_Apocalypto_Album_Cover.jpg">
        <div class="description">
          <h2>Post-Apocalypto</h2>
          <p>Selon le site <a href="https://rateyourmusic.com">rateyourmusic.com</a>
            l'album, Post-Apocalypto, a une note moyenne de: <br> 2,56 sur un plafond de 5 (289 évaluations).<br> Pour
            plus d'information sur les évaluations de l'album visitez: <a
              href="https://rateyourmusic.com/release/album/tenacious-d/post-apocalypto/">https://rateyourmusic.com/release/album/tenacious-d/post-apocalypto</a>
          </p>
        </div>
          <div class="evaluation">
            <p>2,56</p>
            <hr>
            <p>5</p>
        </div>
      </div>

      <div class="statistique">
        <img src="../img/Rize_Of_The_Fenix_Album_Cover.jpg">
        <div class="description">
          <h2>Rize Of The Fenix</h2>
          <p>Selon le site <a href="https://rateyourmusic.com">rateyourmusic.com</a>
            l'album, Rize Of The Fenix, a une note moyenne de: <br> 3,01 sur un plafond de 5 (793 évaluations).<br> Pour
            plus d'information sur les évaluations de l'album visitez: <a
              href="https://rateyourmusic.com/release/album/tenacious_d/rize_of_the_fenix_f1/">https://rateyourmusic.com/release/album/tenacious-d/post-apocalypto</a>
          </p>
        </div>
          <div class="evaluation">
            <p>3,01</p>
            <hr>
            <p>5</p>
        </div>
      </div>

      <div class="statistique">
        <img src="../img/The_Pick_Of_Destiny_Album_Cover.jpg">
        <div class="description">
          <h2>The Pick Of Destiny</h2>
          <p>Selon le site <a href="https://rateyourmusic.com">rateyourmusic.com</a>
            l'album, The Pick Of Destiny, a une note moyenne de: <br> 3,29 sur un plafond de 5 (2015 évaluations).<br>
            Pour plus d'information sur les évaluations de l'album visitez: <a
              href="https://rateyourmusic.com/release/album/tenacious-d/the-pick-of-destiny-1/">https://rateyourmusic.com/release/album/tenacious-d/the-pick-of-destiny-1/</a>
          </p>
        </div>
          <div class="evaluation">
            <p>3,29</p>
            <hr>
            <p>5</p>
        </div>
      </div>

      <div class="statistique">
        <img src="../img/Tenacious_D_Album_Cover.jpg">
        <div class="description">
          <h2>Tenacious D</h2>
          <p>Selon le site <a href="https://rateyourmusic.com">rateyourmusic.com</a>
            l'album, Tenacious D, a une note moyenne de: <br> 3,46 sur un plafond de 5 (3147 évaluations).<br> Pour plus
            d'information sur les évaluations de l'album visitez: <a
              href="https://rateyourmusic.com/release/album/tenacious_d/tenacious_d/">https://rateyourmusic.com/release/album/tenacious_d/tenacious_d/</a>
          </p>
        </div>
          <div class="evaluation">
            <p>3,46</p>
            <hr>
            <p>5</p>
        </div>
      </div>

      <h2>Filmographie</h2>
      <div class="statistique">
        <img src="../img/pick_of_destiny_movie.jpg">
        <div class="description">
        <h2>The Pick Of Destiny (Le Film)</h2>
        <p>Selon le site <a href="https://www.rottentomatoes.com">rottentomatoes.com</a>
            le film, The Pick Of Destiny, a une note moyenne de: <br> 3,67 sur un plafond de 5 (436 403
            évaluations).<br> Pour plus d'information sur les évaluations de l'album visitez: <a
              href="https://www.rottentomatoes.com/m/tenacious_d_in_the_pick_of_destiny">https://www.rottentomatoes.com/m/tenacious_d_in_the_pick_of_destiny</a>
          </p>
        </div>
          <div class="evaluation">
            <p>3,67</p>
            <hr>
            <p>5</p>
        </div>
      </div>

    </div>

  </div>
</div>