<div class="container">
  <main>
    <section>
      <h1>La discographie</h1>
      <section class="discographie">
          <div class="album">
              <p class="annee">2018</p>
              <h1>Post Apocalypto</h1>
              <img src="img/Post_Apocalypto_Album_Cover.jpg" height="42" width="42"/>
              <p>Un extrait de : "Post Apocalypto Theme"</p>
              <audio controls>
                <source src="../audio/Tenacious_D_POST_APOCALYPTO_THEME.mp3" type="audio/mp3"/>

                Votre navigateur ne supporte pas la balise audio.
              </audio>
          </div>
          <div class="album">
              <p class="annee">2012</p>
              <h1>Rize of the Fenix</h1>
              <img src="img/Rize_Of_The_Fenix_Album_Cover.jpg" height="42" width="42"/>
              <p>Un extrait de : "Rize Of The Fenix"</p>
              <audio controls>
                <source src="../audio/Tenacious_D_Rize_of_the_Fenix.mp3" type="audio/mp3"/>

                Votre navigateur ne supporte pas la balise audio.
              </audio>
          </div>
          <div class="album">
              <p class="annee">2006</p>
              <h1>The Pick of Destiny</h1>
              <img src="img/The_Pick_Of_Destiny_Album_Cover.jpg" height="42" width="42"/>
              <p>Un extrait de : "The Metal"</p>
              <audio controls>
                <source src="../audio/The_Metal_Tenacious_D.mp3" type="audio/mp3"/>

                Votre navigateur ne supporte pas la balise audio.
              </audio>
          </div>
          <div class="album">
              <p class="annee">2001</p>
              <h1>Tenacious D</h1>
              <img src="img/Tenacious_D_Album_Cover.jpg" height="42" width="42"/>
              <p>Un extrait de "Tribute"</p>
              <audio controls>
                <source src="../audio/Tenacious_D_Tribute.mp3" type="audio/mp3"/>

                Votre navigateur ne supporte pas la balise audio.
              </audio>
          </div>
    </section>
    <section class="maison">
      <h1>La maison de disque</h1>
      <a href="http://www.columbiarecords.com">Colombia Records</a>
    </section>
  </main>

</div>
