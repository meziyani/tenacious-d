<div class="container">
<div class="apropos">
<h1>À Propos</h1>
<div class="description">
  <p>Tenacious D est un groupe de hard rock satirique américain, rock acoustique, rock comédie, originaire de Los Angeles, en Californie. Le groupe est un duo composé des musiciens et acteurs <a href="http://tp.mezyan.net/index.php?p=6">Jack Black et Kyle Gass</a> qui sont à la fois chanteurs et guitaristes acoustiques. Le groupe est formé en 1994 en tant que duo acoustique. Le groupe devient populaire en 1999 en apparaissant dans la série télévisée du même nom. En 2001, ils sortent l'album Tenacious D, leur premier album enregistré en compagnie d’un groupe composé de figures connues de la scène rock telles que Dave Grohl, chanteur et guitariste des Foo Fighters. Le premier single, Tribute qui a connu un grand succès au yeux des fans et a placé la chanson dans le Top 10 des classements musicaux américains pour la première fois jusqu’à la sortie de la chanson The Metal en 2006 qui, grâce à sa présence dans le jeu vidéo Guitar Hero III, leur permet de revenir sur le devant de la scène et de se faire connaître d’un plus large public.</p>
</div>
<a href="index.php?p=6" class="bouton">Membres</a>
</div>

</div>
